VOWELS = "aeiouy"
CONSONANTS = "bcdfghjklmnpqrstvwxz"
ALPHABET = str().join(sorted(VOWELS+CONSONANTS))
NUMBERS = "0123456789"
SYMBOLS = "`~!@#$%^&*()_+-={}[]:;,.<>/?|\\"+'"'
LEGAL = ALPHABET+ALPHABET.upper()+NUMBERS
ALLCHARS = LEGAL+SYMBOLS

SIDES = [
    "left", "right",
    "front", "behind",
    "top", "bottom",
]

EXITS = {
    "n":"north", "e":"east", "s":"south", "w": "west",
    "u": "up", "d": "down", "i": "in", "o": "out"
}

OPPOSITES = {
    "n":"s","e":"w","s":"n","w":"e",
    "u":"d","d":"u","i":"o","o":"i",
    "l":"r","r":"l", 0 : 1,  1 : 0,
}

COLORS = [
    "red", "orange", "yellow", "green", "cyan",
    "blue", "indigo", "magenta", "violet", "purple",
]

VELOCITIES = {
    "n":(0,-16,12), "s":(0, 16,-12),
    "e":(16, 0,12), "w":(-16, 0,12),
}
