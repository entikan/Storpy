import sys
from time import sleep

from panda3d.core import TextNode
from game.constants import LEGAL

SHITSMICROPOOPY = False
try:
    from msvcrt import getch as rawgetch
    SHITSMICROPOOPY = True
except ImportError:
    def rawgetch():
        import tty
        import termios
        fd = sys.stdin.fileno()
        old = termios.tcgetattr(fd)
        try:
            tty.setraw(fd)
            return sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old)


class IOTerm:
    def __init__(self):
        self.left_to_say = []
        self.input = input

    def say(self, say=" ", wait=True, speed=0.0002, end="\n"):
        self.left_to_say += list(str(say)+end)
        self.speed = speed
        self.write()
        if wait:
            getch()

    def write(self):
        while len(self.left_to_say) > 0:
            a = self.left_to_say.pop(0)
            sys.stdout.write(a)
            sys.stdout.flush()
            sleep(self.speed) #NOT fast enough?

    def clear(self):
        s = '\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n'
        s +='\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n'
        for i in range(32):
            print(s)

    def options(self):
        pass

    def getch(self):
        c = rawgetch()
        if SHITSMICROPOOPY:
            c = "".join(map(chr, c))
        if c == '\x03':
            raise KeyboardInterrupt()
        elif c == '\x1b':
            c = self.getch() + self.getch()
            arrows = {'[A': 'up', '[B': 'down', '[C': 'right', '[D': 'left'}
            if c in arrows:
                c = arrows[c]
        return c


class Message:
    def __init__(self, text, speed=0.05, style="white"):
        self.text = text
        self.remaining = list(text)
        self.speed = speed
        self.style = style


class IOPanda:
    def __init__(self):
        self.saying = None
        self.pressed = []

        self.input_history = []
        self.timer = 0

        self.text = aspect2d.attach_new_node(TextNode("IO"))
        self.text.set_scale(0.06)
        self.text.set_pos(-1,0,0.8)
        self.text.node().set_wordwrap(32)
        self.text.node().set_max_rows(28)
        self.text.node().set_shadow(0.1)

        thrower = base.buttonThrowers[0].node()
        thrower.set_keystroke_event('keystroke')
        base.accept('keystroke', self._stroke)
        base.task_mgr.add(self._update)

    def _stroke(self, key):
        self.pressed.append(key)

    def _update(self, task):
        self.timer += base.clock.dt
        while self.saying and  self.timer >= self.saying.speed:
            self.timer -= self.saying.speed
            if len(self.saying.remaining):
                char = self.saying.remaining.pop(0)
                self.text.node().text += char
            else:
                self.saying = None
        while self.text.node().has_overflow():
            self.text.node().text = self.text.node().text.split('\n', 1)[1]
        if not self.saying:
            self.timer = 0
        return task.cont

    # Call these from thread
    def input(self, cursor="> "):
        out = ""
        self.pressed = []
        self.text.node().text += "\n"+cursor
        while not "\r" in self.pressed:
            if len(self.pressed):
                if self.pressed[0] in LEGAL+" ":
                    out += self.pressed[0]
                    self.text.node().text += self.pressed[0]
                    self.pressed.pop(0)
                elif self.pressed[0] == "\b":
                    if len(out) > 0:
                        out = out[:-1]
                        self.text.node().text = self.text.node().text[:-1]
                    self.pressed.pop(0)
                else:
                    self.pressed = self.pressed[1:] + self.pressed[:1]
        self.text.node().text += "\n"
        self.input_history.append(out)
        return out

    def clear(self):
        self.text.node().text = ""

    def getch(self):
        self.pressed = []
        while not len(self.pressed):
            pass
        key = self.pressed[0]
        self.pressed = []
        return key

    def say(self, text="", wait=True, speed=0.0002, style="white", end="\n"):
        self.saying = Message(text+end, speed, style)
        while self.saying:
            pass
        if wait:
            getch()
