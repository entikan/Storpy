from random import shuffle, choice, randint
from .constants import *


def getoption(options=[]):
    c = getch()
    if c in options:
        return c
    else:
        say("Incorrect option.", wait=False)
        return getoption(options)


# say multiple things
def saym(*strings):
    for s in strings:
        say(s)

# say things slowly, dramatically, with pauses and things
def drama(*strings):
    for s in strings:
        say(s, speed=0.07)

# actions = ["verb/action", function]
def act(*actions):
    say(wait=False)
    options = ""
    for a, action in enumerate(actions):
        say(str(a+1)+" > "+action[0], wait=False)
        options += str(a+1)
    o = int(getoption(options))-1
    say(actions[o][0], wait=False, speed=0.05, end="...\n\n")
    actions[o][1]()

# actions = ["verb/action", *strings to say]
def act_simple(*actions):
    say(wait=False)
    options = ""
    for a, action in enumerate(actions):
        say(str(a+1)+" > "+action[0], False)
        options += str(a+1)
    o = int(getoption(options))-1
    say(actions[o][0], wait=False, speed=0.05, end="...\n\n")
    for s in actions[o][1:]:
        say(s, False)
    say(wait=False)
    return o

# Re-ask actions n-amount of times untill
# n-amount of unique options have been done
def act_counting(actions, n):
    acts = []
    while len(acts) < n:
        o = act_simple(*actions)
        if o not in acts:
            acts.append(o)

# Re-ask actions and pop untill none are left
def act_eliminate(*actions):
    acts = list(actions)
    while len(acts):
        o = act_simple(*acts)
        if o not in acts:
            acts.pop(o)

# actions = ["verb/action", function]
def act_specific(*actions):
    actions = list(actions)
    say(wait=False)
    while True:
      options = ""
      for a, action in enumerate(actions):
          say(str(a+1)+" > "+action[0], wait=False)
          options += str(a+1)
      o = int(getoption(options))-1
      say(actions[o][0], wait=False, speed=0.05, end="...\n\n")
      if callable(actions[o][1]):
        break
      else:
        for s in actions[o][1:]:
          say(s, False)
        actions.pop(o)
    actions[o][1]()

# This displays a list in multiple columns.
# To save on vertical space.
def column_list(option_dict, columns=1, ljust=24):
    per_column = len(option_dict)//columns
    if per_column == 0:
        per_column = 1
    lines = []
    for c in range(per_column):
        lines.append("")
    for o, option in enumerate(option_dict):
        lines[o%per_column] += (option+": "+option_dict[option][0]).ljust(ljust)
    say("\n".join(lines)+"\n", wait=False, speed=0.002)


# This is for things like listing inventory
# Or looking around a room.
def action_dict(actions, show=True, cancel=True):
    if cancel:
        actions["space"] = "Cancel", -1
    if show:
        column_list(actions)
    while True:
        g = getch()
        if g == "?" and not show:
            column_list(actions)
        elif cancel and g == " ":
            say("Cancelled.", wait=False)
            return
        elif g in actions:
            if len(actions[g]) == 1:
                pass
            elif len(actions[g]) > 2:
                actions[g][1](*actions[g][2:])
            else:
                actions[g][1]()
            return
