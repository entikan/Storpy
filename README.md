# Storpy

CYOA-like interactive fiction engine for python using panda3d or print in CLI.


`python main.py` to start in cli mode.
`python main.py 1` to start in graphical mode using panda3d.

Print lines using the builtin "say", and make forks using operations.action.
There's more interesting operations in operations.py.

The graphical mode is async, so it can wait for user input while updating graphical things in the background for things like animation.
