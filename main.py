import sys
import builtins

if __name__ == "__main__":
    panda = False
    if len(sys.argv) > 1:
        panda = sys.argv[1]
    if panda:
        from direct.stdpy import thread
        from direct.showbase.ShowBase import ShowBase

        from panda3d.core import loadPrcFile, Filename

        from storpy.io import IOPanda

        loadPrcFile(Filename.expand_from("$MAIN_DIR/settings.prc"))
        base = ShowBase()
        base.win.set_clear_color((0,0,0,1))
        base.accept("escape", sys.exit)
        io = IOPanda()

        builtins.getch = io.getch
        builtins.say = io.say
        builtins.input = io.input
        builtins.quit = sys.exit

        from storpy.chapter1 import start
        thread.start_new_thread(start, args=[])
        base.run()

    else:
        from storpy.io import IOTerm

        def do_nothing(*kwargs):
            pass

        io = IOTerm()

        builtins.getch = io.getch
        builtins.say = io.say
        builtins.input = io.input
        builtins.quit = sys.exit
        from storpy.chapter1 import start
        start()

